import XCTest
@testable import SwiftyQRGen

final class SwiftyQRGenTests: XCTestCase {
    func testExample() {
        let qrcode = SwiftyQRGen("ayyyy lmao swag dawg")
        qrcode.encodeText()
        var initial: String = ""
        var builder: String = ""
        for i in 0...80 {
            initial += String(qrcode.qrcode[i], radix: 2)
        }
        
        for (n, c) in initial.enumerated() {
            if n % 24 == 0 {
                builder += "\(c)\n"
            } else {
                builder += "\(c)"
            }
        }
        log
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
