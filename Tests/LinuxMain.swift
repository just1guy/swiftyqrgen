import XCTest

import SwiftyQRGenTests

var tests = [XCTestCaseEntry]()
tests += SwiftyQRGenTests.allTests()
XCTMain(tests)
