/**
    Structure that allows Bytes to be represented as an array of Boolean values (binary)

    # Summary
    This structure takes UInt8 (aka UTF8) data supplied to it via characters and produces a binary representation of the data so that it can be easily parsed into a QR Code.
*/
public struct ByteData {
    /// UTF8 representation of input data
    public var rawValue: UInt8
    /// Read only computed value that converts the UTF8 data to an array of Booleans similar to binary
    public var value: [Bool] {
        get {
            var binary = String(rawValue, radix: 2).map { $0 == "1" ? true : false }
            if binary.count < 8 {
                for _ in 1...8 - binary.count {
                    binary.insert(false, at: 0)
                }
            }
            return binary
        }
    }
    public init(_ val: UInt8) {
        self.rawValue = val
    }
}

/**
 Awesome QR Code generator library written in pure Swift
 
 # Usage
 
 */
public class SwiftyQRGen {
    let ecc: [String: [UInt8]] = [
        "low":      [0, 1],
        "medium":   [1, 0],
        "quartile": [2, 3],
        "high":     [3, 2]
    ]
    
    let mode: [String: [Bool]] = [
        "numeric":      [ false, false, false, true  ],
        "alphanumeric": [ false, false, true,  false ],
        "byte":         [ false, true,  false, false ],
        "kanji":        [ true,  false, false, false ],
        "eci":          [ false, true,  true,  true  ]
    ]
    
    let regexLib: [String: String] = [
        "numeric":      "/^[0-9]*$/",
        "alphanumeric": "/^[A-Z0-9 $%*+.\\/:-]*$/"
    ]
    
    private let alphanumericCharset: [Character: UInt8] {
        get {
            var dict: [Character: UInt8] = [:]
            for (n, c) in "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:".enumerated() {
                dict[c] = n
            }
            return dict
        }
    }
    
    let positionPattern = [
        [ true,  true,  true,  true,  true,  true,  true ],
        [ true,  false, false, false, false, false, true ],
        [ true,  false, true,  true,  true,  false, true ],
        [ true,  false, true,  true,  true,  false, true ],
        [ true,  false, true,  true,  true,  false, true ],
        [ true,  false, false, false, false, false, true ],
        [ true,  true,  true,  true,  true,  true,  true ],
    ]
    
    let alignmentPattern = [
        [ true,  true,  true,  true,  true ],
        [ true,  false, false, false, true ],
        [ true,  false, true,  false, true ],
        [ true,  false, false, false, true ],
        [ true,  true,  true,  true,  true ],
    ]
    
    struct Version {
        var version: UInt8
        var size: UInt8 {
            get {
                return version * 4 + 17
            }
        }
        var totalModules: UInt {
            get {
                return size * size
            }
        }
        var alignmentModules: UInt {
            get {
                switch version {
                case 1:
                    return 0
                case 2...6:
                    return 1
                case 7...13:
                    return 6
                case 14...20:
                    return 13
                case 21...27:
                    return 22
                case 28...34:
                    return 33
                case 35...40:
                    return 46
                default:
                    return 0
                }
            }
        }
        var versionInfo: 
        var modulesAvailable: UInt {
            get {
                return totalModules - (8 * 6) - (7 * 7 * 3) - (5 * 5 * alignmentModules) - (2 * size - 16) - (2 * 8 + 2 * 6 + 3) - (version < 7 ? 0 : 3 * 6)
            }
        }
        
        init(_ ver: UInt8) {
            self.version = ver
        }
    }
    
    var version: Version
    
    struct QrSegment {
        let mode: (UInt8, [UInt8])
        let numChars: Int
        let bitData: []
    }
    var segments: [QrSegment]
    
    init() {
        
    }
}
